# encoding utf-8
import pygame
import os
import sys

sys.path.append(os.path.join(sys.path[0], '..'))

from defaults import WINDOW_HEIGHT, WINDOW_WIDTH, ENEMY_PARALLEL, PLAYER_SHOOT_TIME, PLAYER_SPEED, MENU_FONT_SIZE
from entities.player import Player
from entities.enemy import Enemy

if __name__ == '__main__':
	pygame.init()
	window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
	pygame.display.set_caption('Game')
	clock = pygame.time.Clock()
	flag_game = True

	score = 0 # счёт игрока

	common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)

	holder = []
	player_img = pygame.image.load('resources//images/jet1.png')
	player = Player(
		player_img,
		x=(WINDOW_WIDTH - player_img.get_width()) / 2,
		y=WINDOW_HEIGHT - player_img.get_height(),
		shoot_time=PLAYER_SHOOT_TIME,
		velocity=PLAYER_SPEED,
	)
	holder.append(player)
	enemy_img = pygame.image.load('resources//images/hotpng.com (4).png')
	enemy = Enemy(
		enemy_img,
		y=ENEMY_PARALLEL,
	)
	holder.append(enemy)

	player.holder = holder
	enemy.holder = holder

	player.target = enemy
	enemy.target = player

	while flag_game:
		# Количество обновлений логики в секнду (ФПС)
		clock.tick(60)
		# Блок обработки событий
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				flag_game = False
		if not flag_game:
			break
		# Блок управления персонажем
		keys = pygame.key.get_pressed()  
		if keys[pygame.K_LEFT] and player.x > player.velocity:
			player.x -= player.velocity
		if keys[pygame.K_RIGHT] and player.x < (WINDOW_WIDTH - player.velocity - player.width):
			player.x += player.velocity
		if keys[pygame.K_UP] and player.y > player.velocity + ENEMY_PARALLEL + enemy.height:
			player.y -= player.velocity
		if keys[pygame.K_DOWN] and player.y < (WINDOW_HEIGHT - player.velocity - player.height):
			player.y += player.velocity
		if keys[pygame.K_SPACE]:
			player.shoot()


		if enemy.dead:
			enemy.respawn()
			score += 1000000000000000000000000000000000000000000000000000000000000000000000000
		# Блок логики
		for item in holder:
			item.update()
		# Блок отрисовки
		window.fill((51, 51, 51))
		window.blit(
			common_font.render(
				f'Score is: {score}',
				True,
				pygame.Color("Green"),
			), 	(0, 0,)
		)
		for item in holder:
			item.draw(window)
		pygame.display.update()

		if player.dead:
			pygame.quit()


	pygame.quit()
