import pygame
from space_battle.entities.world_object import WorldObject
from space_battle.entities.mortal import Mortal
from space_battle.entities.bullet import Bullet
from space_battle.defaults import SHOOT_TIME, BULLET_SPEED, ENEMY_PARALLEL, DEFAULT_HP, HEALTH_BAR_HEIGHT


class Player(WorldObject, Mortal):
	def __init__(self, image, shoot_time=SHOOT_TIME, *args, **kwargs):
		super(Player, self).__init__(*args, **kwargs)
		self.window = pygame.display.get_surface()
		self.img = image
		self.width = self.img.get_width()
		self.height = self.img.get_height()
		self.last_shoot = 0
		self.shoot_time = shoot_time
		self.target = None
		self.holder = None

	def draw(self, window):
		if self.dead:
			return
		window.blit(self.img, (self.x, self.y))
		pygame.draw.rect(
			window,
			self.color,
			(self.x, self.y + self.height, self.width * (self.hp / DEFAULT_HP), HEALTH_BAR_HEIGHT),
		)


	def shoot(self):
		current_tick = pygame.time.get_ticks()
		if (current_tick - self.last_shoot) > self.shoot_time:
			self.last_shoot = current_tick
			self.weapon()

	def weapon(self):
		return Bullet(
			holder=self.holder,
			velocity=BULLET_SPEED * -1,
			target=self.target,
			x=round(self.x + (self.width / 2)),
			y=self.y,
			color=pygame.Color('red'),
		)
