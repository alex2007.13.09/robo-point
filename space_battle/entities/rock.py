import pygame
from space_battle.entities.world_object import WorldObject
from space_battle.entities.mortal import Mortal


class Rock(WorldObject, Mortal):
	def __init__(self, image, *args, **kwargs):
		super(Rock, self).__init__(*args, **kwargs)
		self.window = pygame.display.get_surface()
		self.img = image
		self.width = self.img.get_width()
		self.height = self.img.get_height()

	def draw(self, window):
		if self.dead:
			return
		window.blit(self.img, (self.x, self.y))
