import pygame
import random

from space_battle.entities.player import Player
from space_battle.entities.bullet import Bullet
from space_battle.defaults import WINDOW_WIDTH, SHOOT_TIME, BULLET_SPEED, ENEMY_PARALLEL, DEFAULT_HP


class Enemy(Player):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.turn = -1

	def update(self):
		# Уничтоденные противники не должны воевать
		if self.dead:
			return
		# Логика движения персонажа
		self.x += self.turn * self.velocity
		if self.x <= self.velocity or (self.x + self.width) >= (WINDOW_WIDTH - self.velocity):
			self.turn *= -1
		# Логика стрельбы персонажа
		self.shoot()

	def update(self):
		# Уничтоденные противники не должны воевать
		if self.dead:
			return
		# При возрождении нужно доставить корабль на позицию
		if self.y >= ENEMY_PARALLEL:
			# Логика движения персонажа
			self.x += self.turn * self.velocity
			if self.x <= self.velocity or (self.x + self.width) >= (WINDOW_WIDTH - self.velocity):
				self.turn *= -1
			# Логика стрельбы персонажа
			self.shoot()
		else:
			self.y += self.velocity

	def weapon(self):
		return Bullet(
			holder=self.holder,
			velocity=BULLET_SPEED,
			target=self.target,
			x=round(self.x + (self.width / 2)),
			y=self.y,
			color=pygame.Color('red'),
		)

	def respawn(self):
		directions = [1, -1]

		self.x = random.randint(self.width, WINDOW_WIDTH - self.width * 2)
		self.y = 0 - self.height
		self.turn = directions[random.randint(0, 1)]
		self.hp = DEFAULT_HP
		self.dead = False

