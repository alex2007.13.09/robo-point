from space_battle.defaults import DEFAULT_HP


class Mortal(object):
	def __init__(self, hp=DEFAULT_HP, *args, **kwargs):
		self.hp = hp
		self.dead = False
		super().__init__(*args, **kwargs)

	def hit(self, damage):
		if self.dead:
			return
		self.hp -= damage
		if self.hp <= 0:
			self.dead = True
