import pygame
import os
import sys

sys.path.append(os.path.join(sys.path[0], '..'))

from defaults import WINDOW_WIDTH, WINDOW_HEIGHT

pygame.init()
window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('Game')
clock = pygame.time.Clock()
flag_game = True
while flag_game:
	clock.tick(60)
	for event in pygame.event.get():
	    if event.type == pygame.QUIT:
	        flag_game = False
	if not flag_game:
	    break

	window.fill((0,0,255))
	pygame.display.update()
	pygame.quit()
