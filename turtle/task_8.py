import turtle
turtle.up()
turtle.left(180)
turtle.forward(270)
turtle.left(180)
turtle.down()
class Random(object):
	def __init__(self, size, color):
		self.size = size
		self.color = color
		turtle.color(self.color)

	def foo(self):
		turtle.down()
		for i in range(4):	
			turtle.forward(self.size)
			turtle.left(90)
		turtle.up()
		turtle.forward(150)
		
	def eoo(self):
		turtle.down()
		for i in range(3):
			turtle.left(120)
			turtle.forward(self.size)
		turtle.up()
		turtle.forward(60)
    
	def coo(self):
		turtle.down()
		turtle.circle(self.size / 2)
		turtle.up()
		turtle.forward(110)

	def yoo(self):
		turtle.down()
		turtle.right(72) 
		for i in range(5):
			turtle.right(144) 
			turtle.forward(self.size)
		turtle.up()
		turtle.left(144)
		turtle.forward(30)	

a = Random (70, "blue")
turtle.begin_fill()
a.foo()
a.eoo()
a.coo()
a.yoo()
turtle.end_fill()

turtle.exitonclick()