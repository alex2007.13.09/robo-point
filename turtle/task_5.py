import turtle
turtle.reset()
turtle.up()
turtle.color("green")
turtle.speed(100)

turtle.left(180)
turtle.forward(300)
turtle.left(180)

turtle.down()

def foo1():
	turtle.down()

	turtle.forward(10)
	turtle.left(90)

	turtle.forward(10)
	turtle.left(90)

	turtle.forward(10)
	turtle.left(90)

	turtle.forward(10)
	turtle.left(90)

	turtle.up()

	turtle.forward(20)

def foo3():
	turtle.down()
	turtle.left(120)
	turtle.forward(10)
	turtle.left(120)
	turtle.forward(10)
	turtle.left(120)
	turtle.forward(10)
	turtle.up()

	turtle.forward(10)

for i in range(20):
	foo1()
	foo3()	

turtle.exitonclick()