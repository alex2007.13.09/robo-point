import pygame
from space_battle.defaults import WINDOW_HEIGHT, WINDOW_WIDTH
from space_battle.ui.stage_machine import StageMachine


pygame.init()
window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('Game')
clock = pygame.time.Clock()

main = StageMachine()
main.run(window, clock)

pygame.quit()
