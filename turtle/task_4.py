import turtle
import math
turtle.reset()
turtle.up()
turtle.left(180)
turtle.forward(150)
turtle.down()

def foo():
	turtle.forward(70)
	turtle.left(90)

	turtle.forward(70)
	turtle.left(90)

	turtle.forward(70)
	turtle.left(90)

	turtle.forward(70)
	turtle.left(90)

def foo2():
	turtle.left(45)
	turtle.forward(70 * math.sqrt(2))
	turtle.left(135)
	turtle.up()
	turtle.forward(70)
	turtle.left(135)
	turtle.down()
	turtle.forward(70 * math.sqrt(2))

turtle.color("red")

foo()
turtle.up()
turtle.left(180)
turtle.forward(70)
turtle.right(90)
turtle.forward(70)
turtle.left(90)
turtle.down()
foo()
foo2()

turtle.up()
turtle.left(45)
turtle.forward(70)

turtle.down()

foo2()
turtle.up()


turtle.exitonclick()